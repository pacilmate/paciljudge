# PacilJudge

[![pipeline status](https://gitlab.com/pacilmate/paciljudge/badges/master/pipeline.svg)](https://gitlab.com/pacilmate/pacilmate/-/commits/master) [![coverage report](https://gitlab.com/pacilmate/paciljudge/badges/master/coverage.svg)](https://gitlab.com/pacilmate/paciljudge/-/commits/master)

PacilJudge is a **Microservice** of PacilMate made with love by our developers. It's a REST API MVC to a grader service!

- Aditya Pratama - 1706039490
- Farah Nazihah - 1906350761
- Hocky Yudhiono - 1906285604
- Muhammad Urwatil Wutsqo - 1906351101
- Wiena Amanda - 1806186591

It is a project made to fulfill the **CSCM602223 - Advanced Programming** course.

Paciljudge is a grader-like microservice. You can add problems, and grade them. **Each user can only submit to a problem once**, and the score will be graded by time and number of wrong submissions as well. **Each user can only solve one problem at a time**.

## API

### Hello World (GET)

`/`

This makes sure the server is active, it will return a string.

```
Hello world!
```

### View problems (GET)

`/view`

This will return an array containing all problems in the database.

```json
[
    {
        "problemId" : "MATH",
        "title" : "Your problem title",
        "question" : "What is 1 + 1?",
        "answer" : "2"
    }
]
```

### Add Problems and Patch problems as well (POST and PUT)

`/add`

This will add a problem instance with this body form, it will also response the entity of the problem. You can also update problems with the same format.

```json
{
    "problemId" : "Code",
    "title" : "Your problem title",
    "question" : "What is the best programming language?",
    "answer" : "java"
}
```

### Init user (POST and PUT)

`/init`

This will add user with certain id and name, this can also acts as an update method.

```json
{
    "userId" : 135363333330042880,
    "username" : "hocky"
}
```

### Solve problem (POST)

`/solve`

Query to queue solve a problem.

```json
{
    "problemId" : "MATH",
    "userId" : 135363333330042880
}
```

A good response will be like.

```json
{
    "sessionId": 123123123123,
    "problem": {
        "problemId": "Cp",
        "title": "Ini judul 2",
        "question": "empat tambah satu berapa?",
        "answer": "lima",
        "solvedBy": []
    },
    "endsAt": {
        "nano": 913658400,
        "year": 2021,
        "monthValue": 5,
        "dayOfMonth": 15,
        "hour": 11,
        "minute": 8,
        "second": 46,
        "month": "MAY",
        "dayOfWeek": "SATURDAY",
        "dayOfYear": 135,
        "chronology": {
            "id": "ISO",
            "calendarType": "iso8601"
        }
    },
    "tries": 0,
    "timeLimit": 300,
    "message": "OK",
    "status": 200
}
```

And a bad response is like this.

```json
{
    "message": "This user can't solve this problem!",
    "status": 400
}
```

### View Sessions (GET)

`/session`

Will acquire all sessions (may be expired, but the availability will be checked when a certain user queries something).

```json
[
    {
        "sessionId": 123123123123,
        "problem": {
            "problemId": "Cp",
            "title": "Ini judul 2",
            "question": "empat tambah satu berapa?",
            "answer": "lima",
            "solvedBy": []
        },
        "endsAt": "2021-05-15T11:08:46.913658",
        "tries": 0,
        "timeLimit": 300
    }
]
```

### View User (GET)

`/user?id={userId}`

Will acquire a user's data with certain id.

```json
{
    "userId": 6969420,
    "username": "hocky",
    "score": 0.0,
    "problemsSolved": [],
    "currentSolve": null,
    "status": 200
}
```

When user is not found it will return this.

```json
{
    "status": 400
}
```

### Release problem (POST)

`/release`

Query to release solve a problem. Request with body:

```json
{
    "userId" : 135363333330042880
}
```

If in db doesn't exist it will return response.

```json
{
    "status": 204
}
```

If in db and released it will return response.

```json
{
    "status": 200
}
```

### Answer Problem (POST)

`/answer`

query to queue solve a problem, request body:

```json
{
    "userId" : 6969420,
    "answer" : "Yes"
}
```

This will return a response body:

```json
{
    "timeRemaining": 295.0,
    "verdict": "ac",
    "score": 98.66666666666667,
    "status": 200
}
```

or

```json
{
    "timeRemaining": 296.0,
    "verdict": "wa",
    "status": 200
}
```

### Check Scoreboard (GET)

`/scoreboard`

This will return the top 10 scorer sorted by score.

```json
[
    {
        "userId": 69694202,
        "username": "hocky",
        "score": 79.0,
        "problemsSolved": [],
        "currentSolve": null
    },
    {
        "userId": 6969420,
        "username": "hocky",
        "score": 19.0,
        "problemsSolved": [],
        "currentSolve": null
    },
    ⋮
    {
        "userId": 69694208,
        "username": "hocky",
        "score": 5.0,
        "problemsSolved": [],
        "currentSolve": null
    },
    {
        "userId": 69694211,
        "username": "hocky",
        "score": 4.0,
        "problemsSolved": [],
        "currentSolve": null
    }
]
```

