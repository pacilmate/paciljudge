package com.paciljudge.paciljudge.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.paciljudge.paciljudge.model.PaciljudgeProblem;
import com.paciljudge.paciljudge.model.PaciljudgeSession;
import com.paciljudge.paciljudge.model.PaciljudgeUser;
import com.paciljudge.paciljudge.service.MainService;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest
@AutoConfigureMockMvc
class MainControllerTests {

    private static final String MESSAGE = "message";
    private static final String STATUS = "status";
    private static final long USER_ID = 696969420L;
    private static final String USER_NAME = "Venti Dewa Mondstadt";
    private static final String PROBLEM_ID = "MONSTA";
    private static final String PROBLEM_TITLE = "Monstar Gaming";
    private static final String PROBLEM_QUESTION = "2 + 2 is 4, 4 - 1 is?";
    private static final String PROBLEM_ANSWER = "3";
    @MockBean
    private MainService mainService;
    @Autowired
    private MockMvc mvc;
    private PaciljudgeProblem paciljudgeProblem;
    private PaciljudgeUser paciljudgeUser;
    private PaciljudgeSession paciljudgeSession;

    @BeforeEach
    void setup() {
        paciljudgeProblem =
            new PaciljudgeProblem(PROBLEM_ID, PROBLEM_TITLE, PROBLEM_QUESTION, PROBLEM_ANSWER);

        paciljudgeUser =
            new PaciljudgeUser(USER_ID, USER_NAME);

        paciljudgeSession =
            new PaciljudgeSession(paciljudgeUser, paciljudgeProblem);
    }

    @Test
    void testHelloWorldPage() throws Exception {
        MvcResult result = mvc.perform(get("/"))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andReturn();

        String content = result.getResponse().getContentAsString();

        assertEquals("Hello world!", content);
    }

    @Test
    void testAddProblemsPage() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        String jsonBody = objectMapper.writeValueAsString(paciljudgeProblem);

        when(mainService.addProblem(any(PaciljudgeProblem.class)))
            .thenReturn(paciljudgeProblem);

        MvcResult result =
            mvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals(jsonBody, result.getResponse().getContentAsString());
    }

    @Test
    void testViewProblems() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        List<PaciljudgeProblem> list = new ArrayList<>();
        list.add(paciljudgeProblem);

        String jsonBody = objectMapper.writeValueAsString(list);

        when(mainService.viewProblems())
            .thenReturn(list);

        MvcResult result = mvc.perform(get("/view"))
            .andExpect(status().isOk())
            .andReturn();

        System.out.println(result.getResponse().getContentAsString());
        assertEquals(jsonBody, result.getResponse().getContentAsString());
    }

    @Test
    void testAddProblemsNullPointer() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put("apa", "ini");
        String jsonBody = mapper.writeValueAsString(jsonNode);

        MvcResult result = mvc.perform(
            post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody)
        ).andExpect(status().isOk()).andReturn();

        JsonNode jsonResult =
            mapper.readTree(result.getResponse().getContentAsString());

        assertEquals("field problemId, title, question, and answer must not be empty",
            jsonResult.get(MESSAGE).asText());
    }

    @Test
    void testInitUserGoesOK() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();
        when(mainService.addUser(anyLong(), anyString())).thenReturn(paciljudgeUser);
        jsonObject.put("userId", USER_ID);
        jsonObject.put("username", USER_NAME);

        MvcResult result = mvc.perform(
            post("/init")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(jsonObject))
        ).andExpect(status().isOk()).andReturn();

        JsonNode jsonResult =
            mapper.readTree(result.getResponse().getContentAsString());

        assertEquals(USER_ID, jsonResult.get("userId").asLong());
    }

    @Test
    void testInitUserGoesNullPointer() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();
        when(mainService.addUser(anyLong(), anyString())).thenReturn(paciljudgeUser);
        jsonObject.put("userId", USER_ID);

        MvcResult result = mvc.perform(
            post("/init")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(jsonObject))
        ).andExpect(status().isOk()).andReturn();

        JsonNode jsonResult =
            mapper.readTree(result.getResponse().getContentAsString());

        assertEquals("Can't add this user to the database!", jsonResult.get(MESSAGE).asText());
    }

    @Test
    void testSolveProblemGoesOk() throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();

        when(mainService.addSession(USER_ID, PROBLEM_ID)).thenReturn(paciljudgeSession);

        jsonObject.put("userId", USER_ID);
        jsonObject.put("problemId", PROBLEM_ID);

        MvcResult result = mvc.perform(
            post("/solve")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(jsonObject))
        ).andExpect(status().isOk()).andReturn();

        JsonNode jsonResult =
            mapper.readTree(result.getResponse().getContentAsString());

        assertEquals(PROBLEM_ID, jsonResult.get("problem").get("problemId").asText());
    }

    @Test
    void testSolveProblemNullPointer() throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();

        when(mainService.addSession(USER_ID, PROBLEM_ID)).thenReturn(paciljudgeSession);

        jsonObject.put("userId", USER_ID);

        MvcResult result = mvc.perform(
            post("/solve")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(jsonObject))
        ).andExpect(status().isOk()).andReturn();

        JsonNode jsonResult =
            mapper.readTree(result.getResponse().getContentAsString());

        assertEquals("This user can't solve this problem!", jsonResult.get(MESSAGE).asText());
    }

    @Test
    void testSolveProblemButAlreadySolved() throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();

        when(mainService.addSession(USER_ID, PROBLEM_ID))
            .thenThrow(new NullPointerException("Already solved"));

        jsonObject.put("userId", USER_ID);
        jsonObject.put("problemId", PROBLEM_ID);

        MvcResult result = mvc.perform(
            post("/solve")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(jsonObject))
        ).andExpect(status().isOk()).andReturn();

        JsonNode jsonResult =
            mapper.readTree(result.getResponse().getContentAsString());

        assertEquals("Already solved", jsonResult.get(MESSAGE).asText());
    }

    @Test
    void testViewSessionsOK() throws Exception {
        when(mainService.viewSessions()).thenReturn(new ArrayList<>());
        mvc.perform(get("/session")).andExpect(status().isOk());
        verify(mainService, times(1)).viewSessions();
    }

    @Test
    void testViewUserOk() throws Exception {
        when(mainService.viewUser(anyLong())).thenReturn(paciljudgeUser);
        mvc.perform(get("/user").param("id", String.valueOf(USER_ID)));
        verify(mainService, times(1)).viewUser(anyLong());
    }

    @Test
    void testViewUserNoUser() throws Exception {
        when(mainService.viewUser(anyLong())).thenReturn(null);
        mvc.perform(get("/user").param("id", String.valueOf(USER_ID)));
        verify(mainService, times(1)).viewUser(anyLong());
    }

    @Test
    void testReleaseProblemOk() throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();

        when(mainService.releaseProblem(anyLong())).thenReturn(200);

        jsonObject.put("userId", USER_ID);

        MvcResult result =
            mvc.perform(
                post("/release")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(jsonObject))
            ).andExpect(status().isOk()).andReturn();

        JsonNode jsonResult =
            mapper.readTree(result.getResponse().getContentAsString());

        assertEquals(200, jsonResult.get(STATUS).asInt());
    }

    @Test
    void testReleaseProblemNullPointerException() throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();

        when(mainService.releaseProblem(anyLong())).thenReturn(200);

        jsonObject.put("bukanUserId", USER_ID);

        MvcResult result =
            mvc.perform(
                post("/release")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(jsonObject))
            ).andExpect(status().isOk()).andReturn();

        JsonNode jsonResult =
            mapper.readTree(result.getResponse().getContentAsString());

        assertEquals(404, jsonResult.get(STATUS).asInt());
    }

    @Test
    void testScoreboardOk() throws Exception {
        when(mainService.scoreboard()).thenReturn(new ArrayList<>());
        mvc.perform(get("/scoreboard")).andExpect(status().isOk());
        verify(mainService, times(1)).scoreboard();
    }

    @Test
    void testAnswerProblemOk() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();

        ObjectNode returnJson = mapper.createObjectNode();
        returnJson.put("verdict", "wa");

        when(mainService.answer(anyLong(), anyString())).thenReturn(returnJson);

        jsonObject.put("userId", USER_ID);
        jsonObject.put("answer", "3");

        MvcResult result =
            mvc.perform(
                post("/answer")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(jsonObject))
            ).andExpect(status().isOk()).andReturn();

        JsonNode jsonResult =
            mapper.readTree(result.getResponse().getContentAsString());

        assertEquals(200, jsonResult.get(STATUS).asInt());
    }

    @Test
    void testAnswerProblemNullPointerException() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonObject = mapper.createObjectNode();

        ObjectNode returnJson = mapper.createObjectNode();
        returnJson.put("verdict", "wa");

        when(mainService.answer(anyLong(), anyString()))
            .thenThrow(new NullPointerException("Not exist session"));

        jsonObject.put("userId", USER_ID);
        jsonObject.put("answer", "3");

        MvcResult result =
            mvc.perform(
                post("/answer")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(jsonObject))
            ).andExpect(status().isOk()).andReturn();

        JsonNode jsonResult =
            mapper.readTree(result.getResponse().getContentAsString());

        assertEquals(404, jsonResult.get(STATUS).asInt());
    }
}
