package com.paciljudge.paciljudge.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import com.paciljudge.paciljudge.model.PaciljudgeProblem;
import com.paciljudge.paciljudge.model.PaciljudgeSession;
import com.paciljudge.paciljudge.model.PaciljudgeUser;
import com.paciljudge.paciljudge.repository.PaciljudgeProblemRepository;
import com.paciljudge.paciljudge.repository.PaciljudgeSessionRepository;
import com.paciljudge.paciljudge.repository.PaciljudgeUserRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
class MainServiceTests {

    private static final long USER_ID = 696969420L;
    private static final String USER_NAME = "Venti Dewa Mondstadt";
    private static final String PROBLEM_ID = "MONSTA";
    private static final String PROBLEM_TITLE = "Monstar Gaming";
    private static final String PROBLEM_QUESTION = "2 + 2 is 4, 4 - 1 is?";
    private static final String PROBLEM_ANSWER = "3";
    @Mock
    PaciljudgeUserRepository paciljudgeUserRepository;
    @Mock
    PaciljudgeSessionRepository paciljudgeSessionRepository;
    @Mock
    PaciljudgeProblemRepository paciljudgeProblemRepository;
    @InjectMocks
    MainServiceImpl mainService;

    private PaciljudgeProblem paciljudgeProblem;
    private PaciljudgeUser paciljudgeUser;
    private PaciljudgeSession paciljudgeSession;

    @BeforeEach
    void setup() {
        paciljudgeProblem =
            new PaciljudgeProblem(PROBLEM_ID, PROBLEM_TITLE, PROBLEM_QUESTION, PROBLEM_ANSWER);

        paciljudgeUser =
            new PaciljudgeUser(USER_ID, USER_NAME);

        paciljudgeSession =
            new PaciljudgeSession(paciljudgeUser, paciljudgeProblem);
    }

    @Test
    void testAddProblemServiceIsGood() {

        when(paciljudgeProblemRepository.findByProblemId(anyString()))
            .thenReturn(paciljudgeProblem);

        when(paciljudgeProblemRepository.save(any(PaciljudgeProblem.class)))
            .thenReturn(paciljudgeProblem);

        mainService.addProblem(paciljudgeProblem);

        verify(paciljudgeProblemRepository, times(1))
            .save(any(PaciljudgeProblem.class));

        verify(paciljudgeProblemRepository, times(1))
            .findByProblemId(anyString());
    }

    @Test
    void testAddProblemServiceIsGoodMakeNew() {

        when(paciljudgeProblemRepository.findByProblemId(anyString()))
            .thenReturn(null);

        when(paciljudgeProblemRepository.save(any(PaciljudgeProblem.class)))
            .thenReturn(paciljudgeProblem);

        mainService.addProblem(paciljudgeProblem);

        verify(paciljudgeProblemRepository, times(1))
            .save(any(PaciljudgeProblem.class));


        verify(paciljudgeProblemRepository, times(1))
            .findByProblemId(anyString());
    }

    @Test
    void testFindAllProblemsIsGood() {

        List<PaciljudgeProblem> list = new ArrayList<>();
        list.add(paciljudgeProblem);

        when(paciljudgeProblemRepository.findAll())
            .thenReturn(list);

        mainService.viewProblems();

        verify(paciljudgeProblemRepository, times(1))
            .findAll();
    }

    @Test
    void testAddUserIsOkThenUpdate() {
        when(paciljudgeUserRepository.findByUserId(anyLong())).thenReturn(paciljudgeUser);
        when(paciljudgeUserRepository.save(any(PaciljudgeUser.class))).thenReturn(paciljudgeUser);

        mainService.addUser(USER_ID, "Ganti");

        assertEquals("Ganti", paciljudgeUser.getUsername());
        verify(paciljudgeUserRepository, times(1)).findByUserId(anyLong());
        verify(paciljudgeUserRepository, times(1)).save(any(PaciljudgeUser.class));

    }

    @Test
    void testAddUserNullThenCreate() {
        when(paciljudgeUserRepository.findByUserId(anyLong())).thenReturn(null);
        when(paciljudgeUserRepository.save(any(PaciljudgeUser.class))).thenReturn(paciljudgeUser);

        mainService.addUser(USER_ID, USER_NAME);

        assertEquals(USER_NAME, paciljudgeUser.getUsername());
        verify(paciljudgeUserRepository, times(1)).findByUserId(anyLong());
        verify(paciljudgeUserRepository, times(1)).save(any(PaciljudgeUser.class));
    }

    @Test
    void testAddSessionOk() {
        when(paciljudgeSessionRepository.findBySessionId(anyLong())).thenReturn(null);
        when(paciljudgeProblemRepository.findByProblemId(anyString()))
            .thenReturn(paciljudgeProblem);
        when(paciljudgeUserRepository.findByUserId(anyLong())).thenReturn(paciljudgeUser);
        when(paciljudgeSessionRepository.save(any(PaciljudgeSession.class)))
            .thenReturn(paciljudgeSession);

        mainService.addSession(USER_ID, PROBLEM_ID);

        verify(paciljudgeSessionRepository, times(1)).findBySessionId(anyLong());
        verify(paciljudgeProblemRepository, times(1)).findByProblemId(anyString());
        verify(paciljudgeUserRepository, times(1)).findByUserId(anyLong());
        verify(paciljudgeSessionRepository, times(1)).save(any(PaciljudgeSession.class));
    }

    @Test
    void testAddSessionAlreadyExists() {
        paciljudgeSession.setEndsAt(LocalDateTime.MAX);
        when(paciljudgeSessionRepository.findBySessionId(anyLong())).thenReturn(paciljudgeSession);

        assertThrows(NullPointerException.class, () -> mainService.addSession(USER_ID, PROBLEM_ID));

        verify(paciljudgeSessionRepository, times(1)).findBySessionId(anyLong());
        verify(paciljudgeProblemRepository, times(0)).findByProblemId(anyString());
        verify(paciljudgeUserRepository, times(0)).findByUserId(anyLong());
        verify(paciljudgeSessionRepository, times(0)).save(any(PaciljudgeSession.class));
    }

    @Test
    void testAddSessionAlreadyExpired() {
        paciljudgeSession.setEndsAt(LocalDateTime.MIN);
        when(paciljudgeSessionRepository.findBySessionId(anyLong())).thenReturn(paciljudgeSession);
        when(paciljudgeProblemRepository.findByProblemId(anyString()))
            .thenReturn(paciljudgeProblem);
        when(paciljudgeUserRepository.findByUserId(anyLong())).thenReturn(paciljudgeUser);
        when(paciljudgeSessionRepository.save(any(PaciljudgeSession.class)))
            .thenReturn(paciljudgeSession);

        mainService.addSession(USER_ID, PROBLEM_ID);

        verify(paciljudgeSessionRepository, times(1)).findBySessionId(anyLong());
        verify(paciljudgeProblemRepository, times(1)).findByProblemId(anyString());
        verify(paciljudgeUserRepository, times(1)).findByUserId(anyLong());
        verify(paciljudgeSessionRepository, times(1)).save(any(PaciljudgeSession.class));
    }

    @Test
    void testAddSessionSolved() {
        paciljudgeSession.setEndsAt(LocalDateTime.MIN);
        paciljudgeUser.getProblemsSolved().add(paciljudgeProblem);
        when(paciljudgeSessionRepository.findBySessionId(anyLong())).thenReturn(paciljudgeSession);
        when(paciljudgeProblemRepository.findByProblemId(anyString()))
            .thenReturn(paciljudgeProblem);
        when(paciljudgeUserRepository.findByUserId(anyLong())).thenReturn(paciljudgeUser);

        assertThrows(NullPointerException.class, () -> mainService.addSession(USER_ID, PROBLEM_ID));

        verify(paciljudgeSessionRepository, times(1)).findBySessionId(anyLong());
        verify(paciljudgeProblemRepository, times(1)).findByProblemId(anyString());
        verify(paciljudgeUserRepository, times(1)).findByUserId(anyLong());
        verify(paciljudgeSessionRepository, times(0)).save(any(PaciljudgeSession.class));
    }

    @Test
    void testAddSessionProblemNotFound() {
        paciljudgeSession.setEndsAt(LocalDateTime.MIN);
        paciljudgeUser.getProblemsSolved().add(paciljudgeProblem);
        when(paciljudgeSessionRepository.findBySessionId(anyLong())).thenReturn(paciljudgeSession);
        when(paciljudgeProblemRepository.findByProblemId(anyString()))
            .thenReturn(null);
        when(paciljudgeUserRepository.findByUserId(anyLong())).thenReturn(paciljudgeUser);

        assertThrows(NullPointerException.class, () -> mainService.addSession(USER_ID, PROBLEM_ID));

        verify(paciljudgeSessionRepository, times(1)).findBySessionId(anyLong());
        verify(paciljudgeProblemRepository, times(1)).findByProblemId(anyString());
        verify(paciljudgeUserRepository, times(1)).findByUserId(anyLong());
        verify(paciljudgeSessionRepository, times(0)).save(any(PaciljudgeSession.class));
    }

    @Test
    void testReleaseProblemSuccess() {
        paciljudgeSession.setEndsAt(LocalDateTime.MAX);
        when(paciljudgeSessionRepository.findBySessionId(anyLong())).thenReturn(paciljudgeSession);
        doNothing().when(paciljudgeSessionRepository).delete(any(PaciljudgeSession.class));
        Integer status = mainService.releaseProblem(USER_ID);

        verify(paciljudgeSessionRepository, times(2)).findBySessionId(anyLong());

        verify(paciljudgeSessionRepository, times(1))
            .delete(any(PaciljudgeSession.class));

        assertEquals(200, status);
    }

    @Test
    void testReleaseProblemExpired() {
        paciljudgeSession.setEndsAt(LocalDateTime.MIN);
        when(paciljudgeSessionRepository.findBySessionId(anyLong())).thenReturn(paciljudgeSession);
        doNothing().when(paciljudgeSessionRepository).delete(any(PaciljudgeSession.class));
        Integer status = mainService.releaseProblem(USER_ID);

        verify(paciljudgeSessionRepository, times(1)).findBySessionId(anyLong());

        verify(paciljudgeSessionRepository, times(1))
            .delete(any(PaciljudgeSession.class));

        assertEquals(204, status);
    }

    @Test
    void testViewUser() {
        when(paciljudgeSessionRepository.findBySessionId(anyLong())).thenReturn(null);
        when(paciljudgeUserRepository.findByUserId(anyLong())).thenReturn(paciljudgeUser);
        mainService.viewUser(USER_ID);
        verify(paciljudgeSessionRepository, times(1)).findBySessionId(anyLong());
        verify(paciljudgeUserRepository, times(1)).findByUserId(anyLong());
    }

    @Test
    void testScoreboard() {
        when(paciljudgeUserRepository.findByOrderByScoreDesc(any(Pageable.class)))
            .thenReturn(new ArrayList<>());
        mainService.scoreboard();
        verify(paciljudgeUserRepository, times(1))
            .findByOrderByScoreDesc(any(Pageable.class));
    }

    @Test
    void testViewSessions() {
        when(paciljudgeSessionRepository.findAll())
            .thenReturn(new ArrayList<>());
        mainService.viewSessions();
        verify(paciljudgeSessionRepository, times(1))
            .findAll();
    }

    @Test
    void testAnswerNoSession() {
        paciljudgeSession.setEndsAt(LocalDateTime.MIN);
        when(paciljudgeSessionRepository.findBySessionId(anyLong())).thenReturn(paciljudgeSession);
        assertThrows(NullPointerException.class, () -> mainService.answer(USER_ID, PROBLEM_ANSWER));
        verify(paciljudgeSessionRepository, times(1)).findBySessionId(anyLong());
    }

    @Test
    void testAnswerAc() {
        when(paciljudgeSessionRepository.findBySessionId(anyLong())).thenReturn(paciljudgeSession);
        mainService.answer(USER_ID, PROBLEM_ANSWER);
        verify(paciljudgeSessionRepository, times(2)).findBySessionId(anyLong());
    }

    @Test
    void testAnswerWa() {
        when(paciljudgeSessionRepository.findBySessionId(anyLong())).thenReturn(paciljudgeSession);
        mainService.answer(USER_ID, "SALAH");
        verify(paciljudgeSessionRepository, times(2)).findBySessionId(anyLong());
    }

}

