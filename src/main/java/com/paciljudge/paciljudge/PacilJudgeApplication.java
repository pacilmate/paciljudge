package com.paciljudge.paciljudge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PacilJudgeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PacilJudgeApplication.class, args);
    }

}
