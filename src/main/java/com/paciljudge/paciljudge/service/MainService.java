package com.paciljudge.paciljudge.service;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.paciljudge.paciljudge.model.PaciljudgeProblem;
import com.paciljudge.paciljudge.model.PaciljudgeSession;
import com.paciljudge.paciljudge.model.PaciljudgeUser;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface MainService {
    PaciljudgeProblem addProblem(PaciljudgeProblem paciljudgeProblem);

    List<PaciljudgeProblem> viewProblems();

    PaciljudgeUser addUser(long userId, String name);

    PaciljudgeSession addSession(long userId, String problemId);

    Integer releaseProblem(long userId);

    ObjectNode answer(long userId, String answer);

    PaciljudgeUser viewUser(long userId);

    List<PaciljudgeUser> scoreboard();

    List<PaciljudgeSession> viewSessions();
}
