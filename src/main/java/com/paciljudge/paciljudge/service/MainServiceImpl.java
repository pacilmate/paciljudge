package com.paciljudge.paciljudge.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.paciljudge.paciljudge.model.PaciljudgeProblem;
import com.paciljudge.paciljudge.model.PaciljudgeSession;
import com.paciljudge.paciljudge.model.PaciljudgeUser;
import com.paciljudge.paciljudge.repository.PaciljudgeProblemRepository;
import com.paciljudge.paciljudge.repository.PaciljudgeSessionRepository;
import com.paciljudge.paciljudge.repository.PaciljudgeUserRepository;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MainServiceImpl implements MainService {

    private static final String STR_VERDICT = "verdict";
    private static final String STR_AC = "ac";
    private static final String STR_WA = "wa";
    private static final String STR_TIME_REMAINING = "timeRemaining";
    private static final String STR_SCORE = "score";
    private static final double INITIAL_SCORE = 100;
    private static final double BASE_SCORE = 20;
    private static final double TRIES_VALUE = 0.90;

    @Autowired
    PaciljudgeProblemRepository paciljudgeProblemRepository;

    @Autowired
    PaciljudgeUserRepository paciljudgeUserRepository;

    @Autowired
    PaciljudgeSessionRepository paciljudgeSessionRepository;

    @Override
    public PaciljudgeProblem addProblem(PaciljudgeProblem paciljudgeProblem) {
        String currentId = paciljudgeProblem.getProblemId();
        PaciljudgeProblem oldProblem = paciljudgeProblemRepository.findByProblemId(currentId);
        PaciljudgeProblem newProblem = (oldProblem == null ? paciljudgeProblem : oldProblem);
        newProblem.setAnswer(paciljudgeProblem.getAnswer());
        newProblem.setQuestion(paciljudgeProblem.getQuestion());
        newProblem.setTitle(paciljudgeProblem.getTitle());
        return paciljudgeProblemRepository.save(paciljudgeProblem);
    }

    @Override
    public List<PaciljudgeProblem> viewProblems() {
        return paciljudgeProblemRepository.findAll();
    }

    @Override
    public PaciljudgeUser addUser(long userId, String name) {
        PaciljudgeUser paciljudgeUser = paciljudgeUserRepository.findByUserId(userId);
        if (paciljudgeUser == null) {
            paciljudgeUser = new PaciljudgeUser(userId, name);
        } else {
            paciljudgeUser.setUsername(name);
        }
        return paciljudgeUserRepository.save(paciljudgeUser);
    }

    @Override
    public PaciljudgeSession addSession(long userId, String problemId) {

        if (deleteIfExpire(userId)) {
            throw new NullPointerException("Already exist active session");
        }

        PaciljudgeUser solver = paciljudgeUserRepository.findByUserId(userId);

        PaciljudgeProblem problem =
            paciljudgeProblemRepository.findByProblemId(problemId);

        if (problem == null) {
            throw new NullPointerException("Problem not found in the database");
        }

        if (solver.getProblemsSolved().contains(problem)) {
            throw new NullPointerException("User has already solved this problem");
        }

        PaciljudgeSession paciljudgeSession = new PaciljudgeSession(solver, problem);

        solver.setCurrentSolve(paciljudgeSession);
        paciljudgeUserRepository.save(solver);

        return paciljudgeSessionRepository.save(paciljudgeSession);
    }

    @Override
    public Integer releaseProblem(long userId) {
        if (!deleteIfExpire(userId)) {
            return 204;
        }
        PaciljudgeSession paciljudgeSession = paciljudgeSessionRepository.findBySessionId(userId);
        paciljudgeSessionRepository.delete(paciljudgeSession);
        return 200;
    }

    @Override
    public ObjectNode answer(long userId, String answer) {

        if (!deleteIfExpire(userId)) {
            throw new NullPointerException("No active session");
        }

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode returnMessage = mapper.createObjectNode();

        PaciljudgeSession paciljudgeSession = paciljudgeSessionRepository.findBySessionId(userId);

        PaciljudgeProblem problem = paciljudgeSession.getProblem();

        double timeRemaining =
            Duration.between(LocalDateTime.now(), paciljudgeSession.getEndsAt()).toSeconds();
        timeRemaining = Math.max(timeRemaining, 0.0);
        returnMessage.put(STR_TIME_REMAINING, timeRemaining);

        if (problem.getAnswer().trim().equalsIgnoreCase(answer.trim())) {

            double penalties = Math.pow(TRIES_VALUE, paciljudgeSession.getTries());
            double scaledInitial = penalties * INITIAL_SCORE;
            double scaledBase = penalties * BASE_SCORE;

            double score = (scaledInitial - scaledBase) * timeRemaining
                / paciljudgeSession.getTimeLimit();

            score += scaledBase;

            PaciljudgeUser solver = paciljudgeSession.getSolver();
            solver.setScore(solver.getScore() + score);
            solver.setCurrentSolve(null);
            solver.getProblemsSolved().add(problem);
            problem.getSolvedBy().add(solver);

            paciljudgeSessionRepository.delete(paciljudgeSession);

            paciljudgeUserRepository.save(solver);
            paciljudgeProblemRepository.save(problem);

            returnMessage.put(STR_VERDICT, STR_AC);
            returnMessage.put(STR_SCORE, score);
        } else {
            returnMessage.put(STR_VERDICT, STR_WA);
            paciljudgeSession.setTries(paciljudgeSession.getTries() + 1);
            paciljudgeSessionRepository.save(paciljudgeSession);
        }
        return returnMessage;
    }


    @Override
    public PaciljudgeUser viewUser(long userId) {
        deleteIfExpire(userId);
        return paciljudgeUserRepository.findByUserId(userId);
    }

    @Override
    public List<PaciljudgeUser> scoreboard() {
        Pageable topTen = PageRequest.of(0, 10);
        return paciljudgeUserRepository.findByOrderByScoreDesc(topTen);
    }

    @Override
    public List<PaciljudgeSession> viewSessions() {
        return paciljudgeSessionRepository.findAll();
    }

    /**
     * Delete a certain session if it expires.
     *
     * @param userId the userId associated
     * @return true if the session is still in the database, false otherwise
     */
    private boolean deleteIfExpire(long userId) {
        PaciljudgeSession paciljudgeSession = paciljudgeSessionRepository.findBySessionId(userId);
        if (paciljudgeSession != null
            && paciljudgeSession.getEndsAt().isBefore(LocalDateTime.now())) {
            paciljudgeSessionRepository.delete(paciljudgeSession);
            return false;
        }
        return paciljudgeSession != null;
    }

}
