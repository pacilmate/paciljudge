package com.paciljudge.paciljudge.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@Table(name = "paciljudge_problem")
public class PaciljudgeProblem {
    @Id
    private String problemId;

    @Column(name = "title")
    private String title;

    @Column(name = "question")
    private String question;

    @Column(name = "answer")
    private String answer;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "solved_list",
        joinColumns = {@JoinColumn(name = "problem_id")},
        inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    @JsonBackReference
    private Set<PaciljudgeUser> solvedBy = new HashSet<>();

    public PaciljudgeProblem(String problemId, String title, String question, String answer) {
        this.problemId = problemId;
        this.title = title;
        this.question = question;
        this.answer = answer;
    }
}
