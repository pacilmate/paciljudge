package com.paciljudge.paciljudge.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@Table(name = "paciljudge_user")
@EqualsAndHashCode(exclude = {"problemsSolved", "currentSolve"})
public class PaciljudgeUser {
    @Id
    private long userId;

    @Column(name = "name")
    private String username;

    @Column(name = "score")
    private double score;

    @ManyToMany(mappedBy = "solvedBy", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<PaciljudgeProblem> problemsSolved = new HashSet<>();

    @OneToOne(mappedBy = "solver")
    @JsonManagedReference
    private PaciljudgeSession currentSolve;

    public PaciljudgeUser(long userId, String username) {
        this.userId = userId;
        this.username = username;
    }
}
