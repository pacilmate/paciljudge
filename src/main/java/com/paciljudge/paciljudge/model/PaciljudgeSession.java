package com.paciljudge.paciljudge.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.time.LocalDateTime;
import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@Table(name = "paciljudge_session")
public class PaciljudgeSession {

    @Id
    private long sessionId;

    @MapsId
    @OneToOne
    @JsonBackReference
    private PaciljudgeUser solver;

    @ManyToOne
    @JoinColumn(name = "session_problem")
    private PaciljudgeProblem problem;

    @Column(name = "end_time")
    private LocalDateTime endsAt;

    @Column(name = "tries")
    private long tries;

    @Column(name = "time_limit")
    private long timeLimit = 300L;

    public PaciljudgeSession(PaciljudgeUser solver, PaciljudgeProblem problem) {
        this.sessionId = solver.getUserId();
        this.solver = solver;
        this.problem = problem;
        this.endsAt = LocalDateTime.now().plusSeconds(timeLimit);
    }
}
