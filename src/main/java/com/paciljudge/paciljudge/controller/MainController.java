package com.paciljudge.paciljudge.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.paciljudge.paciljudge.model.PaciljudgeProblem;
import com.paciljudge.paciljudge.model.PaciljudgeSession;
import com.paciljudge.paciljudge.model.PaciljudgeUser;
import com.paciljudge.paciljudge.service.MainService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MainController {

    private static final String MESSAGE = "message";
    private static final String STATUS = "status";
    private static final String USER_ID = "userId";

    @Autowired
    MainService mainService;

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<String> helloWorld() {
        return ResponseEntity.ok("Hello world!");
    }

    @RequestMapping(path = {"/add"}, produces = {"application/json"}, method = {RequestMethod.PUT,
        RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<ObjectNode> addProblems(
        @RequestBody JsonNode problemNode) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode returnMessage;
        try {
            PaciljudgeProblem paciljudgeProblem =
                new PaciljudgeProblem(
                    problemNode.get("problemId").asText(),
                    problemNode.get("title").asText(),
                    problemNode.get("question").asText(),
                    problemNode.get("answer").asText()
                );
            paciljudgeProblem = mainService.addProblem(paciljudgeProblem);
            returnMessage = mapper.convertValue(paciljudgeProblem, ObjectNode.class);
        } catch (NullPointerException e) {
            returnMessage = mapper.createObjectNode();
            returnMessage.put(MESSAGE,
                "field problemId, title, question, and answer must not be empty");
        }
        return ResponseEntity.ok(returnMessage);
    }

    @GetMapping(path = {"/view"}, produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<List<PaciljudgeProblem>> viewProblems() {
        return ResponseEntity.ok(mainService.viewProblems());
    }

    @RequestMapping(path = {"/init"}, produces = {"application/json"}, method = {RequestMethod.PUT,
        RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<ObjectNode> initUser(@RequestBody(required = false) JsonNode userData) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode returnMessage;
        try {
            PaciljudgeUser paciljudgeUser =
                mainService
                    .addUser(userData.get(USER_ID).asLong(), userData.get("username").asText());
            returnMessage = mapper.convertValue(paciljudgeUser, ObjectNode.class);
        } catch (NullPointerException e) {
            returnMessage = mapper.createObjectNode();
            returnMessage.put(MESSAGE, "Can't add this user to the database!");
        }
        return ResponseEntity.ok(returnMessage);
    }

    @PostMapping(path = {"/solve"}, produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<ObjectNode> solveProblem(
        @RequestBody JsonNode solveData) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode returnMessage;
        try {
            PaciljudgeSession paciljudgeSession =
                mainService.addSession(solveData.get(USER_ID).asLong(),
                    solveData.get("problemId").asText());
            returnMessage = mapper.convertValue(paciljudgeSession, ObjectNode.class);
            returnMessage.put(MESSAGE, "OK");
            returnMessage.put(STATUS, 200);
        } catch (NullPointerException e) {
            returnMessage = mapper.createObjectNode();
            returnMessage.put(MESSAGE, e.getMessage() != null
                ? e.getMessage() :
                "This user can't solve this problem!");
            returnMessage.put(STATUS, 400);
        }
        return ResponseEntity.ok(returnMessage);
    }

    @GetMapping(path = {"/session"}, produces = {"application/json"})
    public ResponseEntity<List<PaciljudgeSession>> showSessions() {
        return ResponseEntity.ok(mainService.viewSessions());
    }

    @GetMapping(path = {"/user"}, produces = {"application/json"})
    public ResponseEntity<ObjectNode> showUser(@RequestParam Long id) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode returnMessage;
        PaciljudgeUser paciljudgeUser = mainService.viewUser(id);
        if (paciljudgeUser != null) {
            returnMessage = mapper.convertValue(paciljudgeUser, ObjectNode.class);
            returnMessage.put(STATUS, 200);
        } else {
            returnMessage = mapper.createObjectNode();
            returnMessage.put(STATUS, 400);
        }
        return ResponseEntity.ok(returnMessage);
    }

    @PostMapping(path = {"/release"}, produces = {"application/json"})
    public ResponseEntity<ObjectNode> releaseProblem(@RequestBody JsonNode releaseData) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode returnMessage = mapper.createObjectNode();
        try {
            Integer result = mainService.releaseProblem(releaseData.get(USER_ID).asLong());
            returnMessage.put(STATUS, result);
        } catch (NullPointerException e) {
            returnMessage = mapper.createObjectNode();
            returnMessage.put(STATUS, 404);
        }
        return ResponseEntity.ok(returnMessage);
    }

    @PostMapping(path = {"/answer"}, produces = {"application/json"})
    public ResponseEntity<ObjectNode> answerProblem(@RequestBody JsonNode answerData) {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode returnMessage = mapper.createObjectNode();
        try {
            returnMessage = mainService
                .answer(answerData.get(USER_ID).asLong(), answerData.get("answer").asText());
            returnMessage.put(STATUS, 200);
        } catch (NullPointerException e) {
            returnMessage.put(STATUS, 404);
        }
        return ResponseEntity.ok(returnMessage);
    }

    @GetMapping(path = {"/scoreboard"}, produces = {"application/json"})
    public ResponseEntity<List<PaciljudgeUser>> showScoreboard() {
        return ResponseEntity.ok(mainService.scoreboard());
    }

}
