package com.paciljudge.paciljudge.repository;

import com.paciljudge.paciljudge.model.PaciljudgeProblem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaciljudgeProblemRepository extends JpaRepository<PaciljudgeProblem, String> {
    PaciljudgeProblem findByProblemId(String id);
}
