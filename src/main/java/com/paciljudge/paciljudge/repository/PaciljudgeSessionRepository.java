package com.paciljudge.paciljudge.repository;

import com.paciljudge.paciljudge.model.PaciljudgeSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaciljudgeSessionRepository extends JpaRepository<PaciljudgeSession, Long> {
    PaciljudgeSession findBySessionId(long userId);

    long countBySessionId(long userId);
}
