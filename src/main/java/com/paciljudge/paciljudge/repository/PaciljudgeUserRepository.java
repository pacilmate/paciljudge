package com.paciljudge.paciljudge.repository;

import com.paciljudge.paciljudge.model.PaciljudgeUser;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaciljudgeUserRepository extends JpaRepository<PaciljudgeUser, Long> {
    PaciljudgeUser findByUserId(long userId);

    List<PaciljudgeUser> findByOrderByScoreDesc(Pageable pageable);
}
